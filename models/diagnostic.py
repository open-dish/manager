from pydantic import BaseModel
from typing import List, Optional

class Observation(BaseModel):
    category: Optional[str]
    code: Optional[str]
    value: Optional[str]
    units: Optional[str]
    type: Optional[str]
    
class Result(BaseModel):
    observation: Optional[List[Observation]]

class Diagnostic(BaseModel):
    id: str
    patient: Optional[str]
    encounter: Optional[str]
    status: Optional[str]
    resultsInterpreter: Optional[str]
    result: Optional[Result]
    study: Optional[List[str]]
    conclusionCode: Optional[List[int]]
    reliability: Optional[List[float]]
