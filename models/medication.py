from pydantic import BaseModel
from typing import Optional

class Medication(BaseModel):
    start: str
    stop: Optional[str]
    patient: Optional[str]
    encounter: Optional[str]
    code: Optional[str]
    description: Optional[str]
    dispenses: Optional[str]
    reasonCode: Optional[str]
    reasonDescription: Optional[str]