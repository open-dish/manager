from pydantic import BaseModel
from typing import List, Optional

class Topleft(BaseModel):
    x: Optional[str]
    y: Optional[str]
    
class Bottomright(BaseModel):
    x: Optional[str]
    y: Optional[str]
    
class Boxes(BaseModel):
    label: Optional[str]
    confidence: Optional[str]
    topLeft: Optional[Topleft]
    bottomRight: Optional[Bottomright]
    
class Image(BaseModel):
    id: str
    date: Optional[str]
    patient: Optional[str]
    encounter: Optional[str]
    imagePath: Optional[str]
    annotationRaw: Optional[str]
    boxesInfo: Optional[List[Boxes]]
    
