from pydantic import BaseModel, Field
from typing import Optional

class Patient(BaseModel):
    id: str
    birthDate: Optional[str]
    deathDate: Optional[str]
    first: Optional[str]
    last: Optional[str]
    race: Optional[str]
    ethnicity: Optional[str]
    gender: Optional[str]
    birthPlace: Optional[str]
    lat: Optional[str]
    lon: Optional[str]