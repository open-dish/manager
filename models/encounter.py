from pydantic import BaseModel
from typing import Optional

class Encounter(BaseModel):
    id: str
    start: Optional[str]
    stop: Optional[str]
    patient: Optional[str]
    participantActor: Optional[str]
    encounterClass: Optional[str]
    code: Optional[str]
    description: Optional[str]
    reasonCode: Optional[str]
    reasonDescription: Optional[str]