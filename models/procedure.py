from pydantic import BaseModel
from typing import Optional

class Procedure(BaseModel):
    start: Optional[str]
    stop: Optional[str]
    patient: Optional[str]
    encounter: Optional[str]
    code: Optional[str]
    reasonCode: Optional[str]
