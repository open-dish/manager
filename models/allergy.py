from pydantic import BaseModel
from typing import Optional

class Allergy(BaseModel):
    start: Optional[str]
    stop: Optional[str]
    patient: Optional[str]
    encounter: Optional[str]
    code: Optional[str]
    system: Optional[str]
    description: Optional[str]
    type: Optional[str]
    category: Optional[str]
    reaction1: Optional[str]
    description1: Optional[str]
    severity1: Optional[str]
    reaction2: Optional[str]
    description2: Optional[str]
    severity2: Optional[str]
