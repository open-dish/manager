from fastapi import APIRouter, HTTPException, Response, status
from models.diagnostic import Diagnostic, Observation
from config.db import db 
from schemas.diagnostic import serializeDict, serializeList
from fastapi.encoders import jsonable_encoder
diagnostic = APIRouter() 

@diagnostic.get('/diagnostic/')
async def find_all_diagnostics(pageNumber: int = 1, nDiagnostic: int = 15):
    total = int(db.command("collstats", "diagnostic")["count"])
    totalPages = total//nDiagnostic+1
    numberDiagnostics = total - nDiagnostic*(totalPages-1) if totalPages-pageNumber == 0 else nDiagnostic
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return [{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Diagnostics": numberDiagnostics },
            serializeList(db.diagnostic.find().skip((pageNumber-1)*nDiagnostic).limit(nDiagnostic))]


@diagnostic.get('/diagnostic/{id}')
async def find_one_diagnostic(id):
    if db.diagnostic.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Diagnostic doesn't exist.")
    return serializeDict(db.diagnostic.find_one({"id":id}))

@diagnostic.get('/diagnostic/patient/{patient_id}')
async def find_diagnostics_by_patient_id(patient_id):
    if db.diagnostic.find_one({"patient":patient_id}) == None:
        raise HTTPException(status_code=404, detail="Diagnostic doesn't exist.")
    return serializeList(db.diagnostic.find(({"patient":patient_id})))
    
@diagnostic.get('/diagnostic/encounter/{encounter_id}')
async def find_diagnostics_by_encounter_id(encounter_id):
    if db.diagnostic.find_one({"encounter":encounter_id}) == None:
        raise HTTPException(status_code=404, detail="Diagnostic doesn't exist.")
    return serializeList(db.diagnostic.find(({"encounter":encounter_id})))

@diagnostic.post('/diagnostic')
async def create_diagnostic(diagnostic: Diagnostic, response: Response):
    test = db.diagnostic.find_one({'id': diagnostic.id});
    if test != None:
        raise HTTPException(status_code=409, detail="Conflict with existing document.")
    else:
        db.diagnostic.insert_one(jsonable_encoder(diagnostic));
        x = db.diagnostic.find_one({'id': diagnostic.id});
        db.diagnostic.delete_one({"id":diagnostic.id});
        x['_id'] = diagnostic.id;
        db.diagnostic.insert_one(x)
        response.status_code = status.HTTP_201_CREATED
        return serializeDict(x)

@diagnostic.put('/diagnostic/{id}')
async def update_diagnostic(id,diagnostic: Diagnostic, response: Response):
    if db.diagnostic.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Diagnostic doesn't exist.")
    db.diagnostic.find_one_and_update({"id":id},{
        "$set": jsonable_encoder(diagnostic)
    })
    response.status_code = status.HTTP_201_CREATED
    return serializeDict(db.diagnostic.find_one({"id":id}))

@diagnostic.delete('/diagnostic/{id}')
async def delete_diagnostic(id):
    if db.diagnostic.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Diagnostic doesn't exist.")
    return serializeDict(db.diagnostic.find_one_and_delete({"id":id}))