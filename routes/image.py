from fastapi import APIRouter, HTTPException, Response, status
from models.image import Image, Boxes
from config.db import db 
from schemas.image import serializeDict, serializeList
from typing import List
from fastapi.encoders import jsonable_encoder
image = APIRouter() 

@image.get('/image/')
async def find_all_images(pageNumber: int = 1, nImage: int = 15):
    total = int(db.command("collstats", "image")["count"])
    totalPages = total//nImage+1
    numberImages = total - nImage*(totalPages-1) if totalPages-pageNumber == 0 else nImage
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return [{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Images": numberImages },
            serializeList(db.image.find().skip((pageNumber-1)*nImage).limit(nImage))]


@image.get('/image/{id}')
async def find_one_image(id):
    if db.image.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Image doesn't exist.")
    return serializeDict(db.image.find_one({"id":id}))

@image.get('/image/patient/{patient_id}')
async def find_images_by_patient(patient_id):
    if db.image.find_one({"patient":patient_id}) == None:
        raise HTTPException(status_code=404, detail="Image doesn't exist.")
    return serializeList(db.image.find({"patient":patient_id}))

@image.get('/image/encounter/{encounter_id}')
async def find_images_by_encounter(encounter_id):
    if db.image.find_one({"encounter":encounter_id}) == None:
        raise HTTPException(status_code=404, detail="Image doesn't exist.")
    return serializeList(db.image.find({"encounter":encounter_id}))

@image.post('/image')
async def create_image(image: Image, response: Response):
    test = db.image.find_one({'id': image.id});
    if test != None:
        raise HTTPException(status_code=409, detail="Conflict with existing document.")
    else:
        db.image.insert_one(jsonable_encoder(image));
        x = db.image.find_one({'id': image.id});
        db.image.delete_one({"id":image.id});
        x['_id'] = image.id;
        db.image.insert_one(x)
        response.status_code = status.HTTP_201_CREATED
        return serializeDict(x)

@image.put('/image/{id}')
async def update_image(id,image: Image):
    if db.image.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Image doesn't exist.")
    db.image.find_one_and_update({"id":id},{
        "$set": jsonable_encoder(image)
    })
    return serializeDict(db.image.find_one({"id":id}))

@image.put('/image/box/{id}')
async def insert_bounding_box(id,box: List[Boxes], response: Response):
    img = db.image.find_one({"id":id})
    if img == None:
        raise HTTPException(status_code=404, detail="Image doesn't exist.")
    if img["boxesInfo"] == None:
        img["boxesInfo"] = jsonable_encoder(box)
    else:
        img["boxesInfo"] = img["boxesInfo"] + jsonable_encoder(box)
    db.image.find_one_and_update({"id":id},{
        "$set":{
            "boxesInfo": img["boxesInfo"]
        }
    })
    response.status_code = status.HTTP_201_CREATED
    return serializeDict(db.image.find_one({"id":id}))

@image.delete('/image/{id}')
async def delete_image(id):
    if db.image.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Image doesn't exist.")
    return serializeDict(db.image.find_one_and_delete({"id":id}))