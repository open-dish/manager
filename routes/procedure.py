from fastapi import APIRouter, HTTPException, Response, status
from models.procedure import Procedure
from config.db import db 
from schemas.procedure import serializeDict, serializeList
from fastapi.encoders import jsonable_encoder
from bson import ObjectId

procedure = APIRouter() 

@procedure.get('/procedure/')
async def find_all_procedures(pageNumber: int = 1, nProcedure: int = 15):
    total = int(db.command("collstats", "procedure")["count"])
    totalPages = total//nProcedure+1
    numberProcedures = total - nProcedure*(totalPages-1) if totalPages-pageNumber == 0 else nProcedure
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return [{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Procedures": numberProcedures },
            serializeList(db.procedure.find().skip((pageNumber-1)*nProcedure).limit(nProcedure))]


@procedure.get('/procedure/patient/{patient_id}')
async def find_procedures_by_patient_id(patient_id):
    if db.procedure.find_one({"patient":patient_id}) == None:
        raise HTTPException(status_code=404, detail="Procedure doesn't exist.")
    return serializeList(db.procedure.find(({"patient":patient_id})))
    
@procedure.get('/procedure/encounter/{encounter_id}')
async def find_procedures_by_encounter_id(encounter_id):
    if db.procedure.find_one({"encounter":encounter_id}) == None:
        raise HTTPException(status_code=404, detail="Procedure doesn't exist.")
    return serializeList(db.procedure.find(({"encounter":encounter_id})))

@procedure.post('/procedure')
async def create_procedure(procedure: Procedure, response: Response):
    db.procedure.insert_one(jsonable_encoder(procedure))
    response.status_code = status.HTTP_201_CREATED
    return serializeDict(jsonable_encoder(procedure))

@procedure.put('/procedure/{id}')
async def update_procedure(id,procedure: Procedure):
    if db.procedure.find_one({"_id":ObjectId(id)}) == None:
        raise HTTPException(status_code=404, detail="Procedure doesn't exist.")
    db.procedure.find_one_and_update({"_id":ObjectId(id)},{
        "$set":dict(procedure)
    })
    return serializeDict(db.procedure.find_one({"_id":ObjectId(id)}))

@procedure.delete('/procedure/{id}')
async def delete_procedure(id):
    if db.procedure.find_one({"_id":ObjectId(id)}) == None:
        raise HTTPException(status_code=404, detail="Procedure doesn't exist.")
    return serializeDict(db.procedure.find_one_and_delete({"_id":ObjectId(id)}))
