from fastapi import APIRouter, HTTPException, Response, status
from models.encounter import Encounter 
from config.db import db 
from schemas.encounter import serializeDict, serializeList
encounter = APIRouter() 

@encounter.get('/encounter/')
async def find_all_encounters(pageNumber: int = 1, nEncounter: int = 15):
    total = int(db.command("collstats", "encounter")["count"])
    totalPages = total//nEncounter+1
    numberEncounters = total - nEncounter*(totalPages-1) if totalPages-pageNumber == 0 else nEncounter
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return [{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Encounters": numberEncounters },
            serializeList(db.encounter.find().skip((pageNumber-1)*nEncounter).limit(nEncounter))]


@encounter.get('/encounter/{id}')
async def find_one_encounter(id):
    if db.encounter.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Encounter doesn't exist.")
    return serializeDict(db.encounter.find_one({"id":id}))

@encounter.get('/encounter/patient/{patient_id}')
async def find_encounter_by_patient_id(patient_id):
    if db.encounter.find_one({"patient":patient_id}) == None:
        raise HTTPException(status_code=404, detail="Encounter doesn't exist.")
    return serializeList(db.encounter.find(({"patient":patient_id})))

@encounter.post('/encounter')
async def create_encounter(encounter: Encounter, response: Response):
    test = db.encounter.find_one({'id': encounter.id});
    if test != None:
        raise HTTPException(status_code=409, detail="Conflict with existing document.")
    else:
        db.encounter.insert_one(dict(encounter));
        x = db.encounter.find_one({'id': encounter.id});
        db.encounter.delete_one({"id":encounter.id});
        x['_id'] = encounter.id;
        db.encounter.insert_one(x)
        response.status_code = status.HTTP_201_CREATED
        return serializeDict(x)

@encounter.put('/encounter/{id}')
async def update_encounter(id,encounter: Encounter):
    if db.encounter.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Encounter doesn't exist.")
    db.encounter.find_one_and_update({"id":id},{
        "$set":dict(encounter)
    })
    return serializeDict(db.encounter.find_one({"id":id}))

@encounter.delete('/encounter/{id}')
async def delete_encounter(id):
    if db.encounter.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Encounter doesn't exist.")
    return serializeDict(db.encounter.find_one_and_delete({"id":id}))