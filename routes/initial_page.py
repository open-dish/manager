from fastapi import APIRouter

initial_page = APIRouter() 

@initial_page.get('/')
async def home():
    return ("Welcome to the Open-dish Suco's REST API", "Enter /docs route to see the API Documentation")