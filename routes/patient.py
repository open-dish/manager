from fastapi import APIRouter, HTTPException, Response, status
from models.patient import Patient 
from config.db import db 
from schemas.patient import serializeDict, serializeList

patient = APIRouter() 

@patient.get('/patient/')
async def find_all_patients(pageNumber: int = 1, nPatient: int = 15):
    total = int(db.command("collstats", "patient")["count"])
    totalPages = total//nPatient+1
    numberPatients = total - nPatient*(totalPages-1) if totalPages-pageNumber == 0 else nPatient
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return [{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Patients": numberPatients },
            serializeList(db.patient.find().skip((pageNumber-1)*nPatient).limit(nPatient))]


@patient.get('/patient/{id}')
async def find_one_patient(id):
    if db.patient.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Patient doesn't exist.")
    return serializeDict(db.patient.find_one({"id":id}))

@patient.post('/patient')
async def create_patient(patient: Patient, response: Response):
    test = db.patient.find_one({'id': patient.id});
    if test != None:
        raise HTTPException(status_code=409, detail="Conflict with existing document.")
    else:
        db.patient.insert_one(dict(patient));
        x = db.patient.find_one({'id': patient.id});
        db.patient.delete_one({"id":patient.id});
        x['_id'] = patient.id;
        db.patient.insert_one(x)
        response.status_code = status.HTTP_201_CREATED
        return serializeDict(x)

@patient.put('/patient/{id}')
async def update_patient(id,patient: Patient):
    if db.patient.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Patient doesn't exist.")
    db.patient.find_one_and_update({"id":id},{
        "$set":dict(patient)
    })
    return serializeDict(db.patient.find_one({"id":id}))

@patient.delete('/patient/{id}')
async def delete_patient(id):
    if db.patient.find_one({"id":id}) == None:
        raise HTTPException(status_code=404, detail="Patient doesn't exist.")
    return serializeDict(db.patient.find_one_and_delete({"id":id}))