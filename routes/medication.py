from fastapi import APIRouter, HTTPException, Response, status
from models.medication import Medication
from config.db import db 
from schemas.medication import serializeDict, serializeList
from fastapi.encoders import jsonable_encoder
from bson import ObjectId

medication = APIRouter() 

@medication.get('/medication/')
async def find_all_medications(pageNumber: int = 1, nMedication: int = 15):
    total = int(db.command("collstats", "medication")["count"])
    totalPages = total//nMedication+1
    numberMedications = total - nMedication*(totalPages-1) if totalPages-pageNumber == 0 else nMedication
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return [{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Medications": numberMedications },
            serializeList(db.medication.find().skip((pageNumber-1)*nMedication).limit(nMedication))]


@medication.get('/medication/patient/{patient_id}')
async def find_medications_by_patient_id(patient_id):
    if db.medication.find_one({"patient":patient_id}) == None:
        raise HTTPException(status_code=404, detail="Medication doesn't exist.")
    return serializeList(db.medication.find(({"patient":patient_id})))
    
@medication.get('/medication/encounter/{encounter_id}')
async def find_medications_by_encounter_id(encounter_id):
    if db.medication.find_one({"encounter":encounter_id}) == None:
        raise HTTPException(status_code=404, detail="Medication doesn't exist.")
    return serializeList(db.medication.find(({"encounter":encounter_id})))

@medication.post('/medication')
async def create_medication(medication: Medication, response: Response):
    db.medication.insert_one(jsonable_encoder(medication))
    response.status_code = status.HTTP_201_CREATED
    return serializeDict(jsonable_encoder(medication))

@medication.put('/medication/{id}')
async def update_medication(id,medication: Medication):
    if db.medication.find_one({"_id":ObjectId(id)}) == None:
        raise HTTPException(status_code=404, detail="Allergy doesn't exist.")
    db.medication.find_one_and_update({"_id":ObjectId(id)},{
        "$set":dict(medication)
    })
    return serializeDict(db.medication.find_one({"_id":ObjectId(id)}))

@medication.delete('/medication/{id}')
async def delete_medication(id):
    if db.medication.find_one({"_id":ObjectId(id)}) == None:
        raise HTTPException(status_code=404, detail="Allergy doesn't exist.")
    return serializeDict(db.medication.find_one_and_delete({"_id":ObjectId(id)}))
