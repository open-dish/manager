from fastapi import APIRouter, HTTPException, Response, status
from models.allergy import Allergy
from config.db import db 
from schemas.allergy import serializeDict, serializeList
from fastapi.encoders import jsonable_encoder
from bson import ObjectId

allergy = APIRouter() 

@allergy.get('/allergy/')
async def find_all_allergys(pageNumber: int = 1, nAllergy: int = 15):
    total = int(db.command("collstats", "allergy")["count"])
    totalPages = total//nAllergy+1
    numberAllergys = total - nAllergy*(totalPages-1) if totalPages-pageNumber == 0 else nAllergy
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return [{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Allergys": numberAllergys },
            serializeList(db.allergy.find().skip((pageNumber-1)*nAllergy).limit(nAllergy))]


@allergy.get('/allergy/patient/{patient_id}')
async def find_allergys_by_patient_id(patient_id):
    if db.allergy.find_one({"patient":patient_id}) == None:
        raise HTTPException(status_code=404, detail="Allergy doesn't exist.")
    return serializeList(db.allergy.find(({"patient":patient_id})))
    
@allergy.get('/allergy/encounter/{encounter_id}')
async def find_allergys_by_encounter_id(encounter_id):
    if db.allergy.find_one({"encounter":encounter_id}) == None:
        raise HTTPException(status_code=404, detail="Allergy doesn't exist.")
    return serializeList(db.allergy.find(({"encounter":encounter_id})))

@allergy.post('/allergy')
async def create_allergy(allergy: Allergy, response: Response):
    db.allergy.insert_one(jsonable_encoder(allergy))
    response.status_code = status.HTTP_201_CREATED
    return serializeDict(jsonable_encoder(allergy))

@allergy.put('/allergy/{id}')
async def update_allergy(id,allergy: Allergy):
    if db.allergy.find_one({"_id":ObjectId(id)}) == None:
        raise HTTPException(status_code=404, detail="Allergy doesn't exist.")
    db.allergy.find_one_and_update({"_id":ObjectId(id)},{
        "$set":dict(allergy)
    })
    return serializeDict(db.allergy.find_one({"_id":ObjectId(id)}))

@allergy.delete('/allergy/{id}')
async def delete_allergy(id):
    if db.allergy.find_one({"_id":ObjectId(id)}) == None:
        raise HTTPException(status_code=404, detail="Allergy doesn't exist.")
    return serializeDict(db.allergy.find_one_and_delete({"_id":ObjectId(id)}))
