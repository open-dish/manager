# Normal way
def imageEntity(item) -> dict:
    return {
        "id": item["id"],
        "date": item["date"],
        "patient": item["patient"],
        "encounter": item["encounter"],
        "imagePath": item["imagePath"],
        "annotationRaw": item["annotationRaw"],
        "boxesInfo": {
            "label": item["label"],
            "confidence": item["confidence"],
            "topLeft":{
                "x": item["x"],
                "y": item["y"]
            },
            "bottomRight":{
                "x": item["x"],
                "y": item["y"]
            }
        }
    }

def imagesEntity(entity) -> list:
    return [imageEntity(item) for item in entity]
#Best way

def serializeDict(a) -> dict:
    return {**{i:str(a[i]) for i in a if i=='_id'},**{i:a[i] for i in a if i!='_id'}}

def serializeList(entity) -> list:
    return [serializeDict(a) for a in entity]