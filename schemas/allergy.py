# Normal way
def allergyEntity(item) -> dict:
    return {
        "start": item["start"],
        "stop": item["stop"],
        "patient": item["patient"],
        "encounter": item["encounter"],
        "code": item["code"],
        "system": item["system"],
        "description": item["description"],
        "type": item["type"],
        "category": item["category"],
        "reaction1": item["reaction1"],
        "description1": item["description1"],
        "severity1": item["severity1"],
        "reaction2": item["reaction2"],
        "description2": item["description2"],
        "severity2": item["severity2"] 
    }

def allergysEntity(entity) -> list:
    return [allergyEntity(item) for item in entity]
#Best way

def serializeDict(a) -> dict:
    return {**{i:str(a[i]) for i in a if i=='_id'},**{i:a[i] for i in a if i!='_id'}}

def serializeList(entity) -> list:
    return [serializeDict(a) for a in entity]