# Normal way
def patientEntity(item) -> dict:
    return {
        "id": item["id"],
        "birthDate": item["birthDate"],
        "deathDate": item["deathDate"],
        "first": item["first"],
        "last": item["last"],
        "race": item["race"],
        "ethnicity": item["ethnicity"],
        "gender": item["gender"],
        "birthPlace": item["birthPlace"],
        "lat": item["lat"],
        "lon": item["lon"]
    }

def patientsEntity(entity) -> list:
    return [patientEntity(item) for item in entity]
#Best way

def serializeDict(a) -> dict:
    return {**{i:str(a[i]) for i in a if i=='_id'},**{i:a[i] for i in a if i!='_id'}}

def serializeList(entity) -> list:
    return [serializeDict(a) for a in entity]