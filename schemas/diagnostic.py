# Normal way
def diagnosticEntity(item) -> dict:
    return {
        "id": item["id"],
        "patient": item["patient"],
        "encounter": item["encounter"],
        "status": item["status"],
        "resultsInterpreter": item["resultsInterpreter"],
        "result": {
            "observation": {
                "category": item["category"],
                "code": item["code"],
                "value": item["value"],
                "units": item["units"],
                "type": item["type"],
            },
        },
        "study": item["study"],
        "conclusionCode": item["conclusionCode"],
        "reliability": item["reliability"],
    }

def diagnosticEntity(entity) -> list:
    return [diagnosticEntity(item) for item in entity]
#Best way

def serializeDict(a) -> dict:
    return {**{i:str(a[i]) for i in a if i=='_id'},**{i:a[i] for i in a if i!='_id'}}

def serializeList(entity) -> list:
    return [serializeDict(a) for a in entity]