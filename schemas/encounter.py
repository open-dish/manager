# Normal way
def encounterEntity(item) -> dict:
    return {
        "id": item["id"],
        "start": item["start"],
        "stop": item["stop"],
        "patient": item["patient"],
        "participantactor": item["participantactor"],
        "encounterClass": item["encounterClass"],
        "code": item["code"],
        "description": item["description"],
        "reasonCode": item["reasonCode"],
        "reasonDescription": item["reasonDescription"],
    }

def encountersEntity(entity) -> list:
    return [encounterEntity(item) for item in entity]
#Best way

def serializeDict(a) -> dict:
    return {**{i:str(a[i]) for i in a if i=='_id'},**{i:a[i] for i in a if i!='_id'}}

def serializeList(entity) -> list:
    return [serializeDict(a) for a in entity]