# Normal way
def medicationEntity(item) -> dict:
    return {
        "start": item["start"],
        "stop": item["stop"],
        "patient": item["patient"],
        "encounter": item["encounter"],
        "code": item["code"],
        "description": item["description"],
        "dispenses": item["dispenses"],
        "reasonCode": item["reasonCode"],
        "reasonDescription": item["reasonDescription"]
    }

def medicationsEntity(entity) -> list:
    return [medicationEntity(item) for item in entity]
#Best way

def serializeDict(a) -> dict:
    return {**{i:str(a[i]) for i in a if i=='_id'},**{i:a[i] for i in a if i!='_id'}}

def serializeList(entity) -> list:
    return [serializeDict(a) for a in entity]