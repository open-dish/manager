# Fastapi MongoDB REST API

To run the REST API, follow the steps:

* Execute the build.sh script to build the Docker Container

* Execute the start.sh script to run the container

* Execute the stop.sh to stop using the container

