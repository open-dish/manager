from fastapi import FastAPI
from routes.initial_page import initial_page
from routes.patient import patient 
from routes.encounter import encounter
from routes.image import image
from routes.diagnostic import diagnostic
from routes.medication import medication
from routes.allergy import allergy
from routes.procedure import procedure
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()

origins = [
    os.getenv("PATIENT_MANAGER_HOST"),
    os.getenv("CONTAINER_HOST"),
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(initial_page)
app.include_router(patient)
app.include_router(encounter)
app.include_router(image)
app.include_router(diagnostic)
app.include_router(medication)
app.include_router(allergy)
app.include_router(procedure)