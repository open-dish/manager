#Remove old container and image
docker rm -vf api_container
docker rmi api_image

#Build image
docker build -t api_image:latest .

docker run -p 5000:5000 -d --name api_container api_image
docker stop api_container