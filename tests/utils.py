import os
import requests
import json
import cerberus

BASE_URL = "https://dhim.api.open-dish.lab.ic.unicamp.br"
DETAIL_SCHEMA = "./schemas/detail_schema.json"


def make_request(endpoint, requestType, obj = {}): #makes the request operations
    if requestType == 'GET':
        response = requests.get(endpoint)
    elif requestType == 'POST':
        response = requests.post(endpoint, json=obj)

    responseJson = response.json()

    if type(responseJson) == list:
        responseJson = {"response": responseJson}

    try:
        return responseJson, response.status_code
    except:
        return {}, response.status_code


def validate_schema(self):
    obj = {}
    if self.request_type == 'POST':
        with open(self.object_file) as f:
            obj = json.load(f)
    with open(self.schema_file) as file:
        data = json.load(file)
        schema = data
        validator = cerberus.Validator(schema)
        validator.allow_unknown = True
        res, code = make_request(self.endpoint, self.request_type, obj)


        if ("response" in self.metrics):
            # with open(DETAIL_SCHEMA) as f:
            #     obj = json.load(f)

            val = validator.validate(res,schema)

            if validator.errors != {}:
                print(validator.errors)
            else:
                print("Schema OK!")
            self.assertEqual(val,True)
        if ("code" in self.metrics):
            if type(self.expected_code) == list:
                good_code = self.expected_code[0]
                for exp_code in self.expected_code:
                    if exp_code == code:
                        good_code = exp_code
                self.assertEqual(code, good_code)
            else:
                self.assertEqual(code,self.expected_code)
    return

def get_test_file_name(file):
    name = os.path.basename(file)
    name = name[5:]
    name = name[0:-3]
    return name
