import unittest
from utils import get_test_file_name, validate_schema
import os


class TestPostProcedures(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestPostProcedures, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.object_file = "./objects/obj_{}.json".format(get_test_file_name(__file__))
        self.endpoint = "http://{}/procedure".format(os.getenv('API_HOST'))
        self.request_type = "POST"
        self.expected_code = [201, 409]
        self.metrics = ["code"]

    def test_schema(self):
        validate_schema(self)
