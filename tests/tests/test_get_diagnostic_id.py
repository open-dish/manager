import unittest
from utils import get_test_file_name, validate_schema
import os


class TestGetDiagnostic(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestGetDiagnostic, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = "http://{}/diagnostic/42bfe9b8-3f5a-11ed-b878-0242ac120002".format(os.getenv('API_HOST'))
        self.request_type = "GET"
        self.expected_code = [200, 404]
        self.metrics = ["response", "code"]

    def test_schema(self):
        validate_schema(self)
