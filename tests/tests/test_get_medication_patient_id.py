import unittest
from utils import get_test_file_name, validate_schema
import os


class TestGetMedications(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestGetMedications, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = "http://{}/medication/patient/58b54dd5-4f3a-487e-169c-1f85402e260a".format(os.getenv('API_HOST'))
        self.request_type = "GET"
        self.expected_code = [200, 404]
        self.metrics = ["response", "code"]

    def test_schema(self):
        validate_schema(self)
