import unittest
from utils import get_test_file_name, validate_schema
import os


class TestGetPatient(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestGetPatient, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.endpoint = "http://{}/patient/?pageNumber=1&nPatient=15".format(os.getenv('API_HOST'))
        self.request_type = "GET"
        self.expected_code = 200
        self.metrics = ["response", "code"]

    def test_schema(self):
        validate_schema(self)
