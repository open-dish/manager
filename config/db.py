import os
from pymongo import MongoClient

# Get mongodb credentials using environment variables
usr = os.getenv("MONGODB_USER", "mc857")
psw = os.getenv("MONGODB_PASSWD", "opendish857")
host = os.getenv("MONGODB_HOST", "cluster0.npvgvco.mongodb.net")
database_name = os.getenv("MONGDB_DBNAME", 'database')
protocol = "mongodb" if host != "cluster0.npvgvco.mongodb.net" else "mongodb+srv"

# Connect to mongodb
conn = MongoClient(f"{protocol}://{usr}:{psw}@{host}/?retryWrites=true&w=majority")

# Get the database
db = conn[database_name]